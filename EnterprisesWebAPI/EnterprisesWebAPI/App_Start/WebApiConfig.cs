﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace EnterprisesWebAPI
{
    public static class WebApiConfig
    {
        private static string apiVersion = "v1";
        public static string tokenPath = "/api/token";

        public static void Register(HttpConfiguration config)
        {
            // Serviços e configuração da API da Web

            // Rotas da API da Web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "Enterprises",
                routeTemplate: "api/{apiVersion}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
            name: "Auth",
            routeTemplate: "api/{apiVersion}/users/auth/sign_in",
            defaults: new { id = RouteParameter.Optional, controller = "sign_in" }
            );
        }
    }
}
