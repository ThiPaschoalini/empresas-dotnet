﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnterprisesWebAPI.Controllers
{

    public class enterprisesController : ApiController
    {
        public bool Validate()
        {
            if (Request.Headers.Contains("uid") && Request.Headers.Contains("client") && Request.Headers.Contains("access-token"))
            {
                if (Request.Headers.GetValues("uid").FirstOrDefault<string>() != null && Request.Headers.GetValues("client").FirstOrDefault<string>() != null && Request.Headers.GetValues("access-token").FirstOrDefault<string>() != null)
                {
                    return true;
                }
            }
            return false;
        }
        public IEnumerable<string> Get()
        {
            if (Validate())
                return new string[] { "value1 - Enterprise", "value2 - Enterprise" };
            else
                return null;
        }

        public string Get(int id)
        {
            return "value";
        }
    }
}
