﻿using EnterprisesWebAPI.Models;
using EnterprisesWebAPI.Provider;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;

namespace EnterprisesWebAPI.Controllers
{
    public class sign_inController : ApiController
    {
        private static readonly HttpClient client = new HttpClient();


        public async Task<HttpResponseMessage> Post([FromBody]SignInRequest value)
        {
            var values = new Dictionary<string, string>
            {
             { "username", value.email },
             { "password", value.password },
                {"grant_type", "password" }
            };

            var content = new FormUrlEncodedContent(values);

            HttpResponseMessage response = await client.PostAsync("http://localhost:55495/api/token", content);

            if (response.IsSuccessStatusCode)
            {
                Persistence db = new Persistence();
                InvestorResponse investor = db.Login(value.email, value.password);


                ResponseData respData = new ResponseData();
                respData.investor = investor;
                respData.enterprise = null;
                respData.success = true;

                string headerResult = await response.Content.ReadAsStringAsync();
                HeaderSignIn headers = JsonConvert.DeserializeObject<HeaderSignIn>(headerResult);

                string hehe = headerResult;
                string test = headers.access_token;

                response.Content = new StringContent(JsonConvert.SerializeObject(respData));


                response.Headers.Add("login", (investor != null) ? "True" : "False");
                response.Headers.Add("uid", value.email);
                response.Headers.Add("access-token", headers.access_token);
                response.Headers.Add("token-type", headers.token_type);
                response.Headers.Add("expiry", headers.expires_in.ToString());

            }
            else
            {
                HttpResponseMessage erro = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                erro.Content = new StringContent("ERRO");
                return erro;
            }



            return response;
        }
    }
}
