﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterprisesWebAPI.Models
{
    public class Portfolio
    {
        public int enterprises_number { get; set; }
        public string[] enterprises { get; set; }

        public Portfolio()
        {
            this.enterprises_number = 0;
            this.enterprises = new string[0];
        }
    }
}