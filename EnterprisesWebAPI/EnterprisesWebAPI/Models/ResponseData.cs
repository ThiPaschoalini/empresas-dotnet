﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterprisesWebAPI.Models
{
    public class ResponseData
    {
        public InvestorResponse investor { get; set; }
        public Enterprise enterprise { get; set; }
        public bool success { get; set; }

    }
}