﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterprisesWebAPI.Models
{
    public class SignInRequest
    {
        public string email { get; set; }
        public string password { get; set; }
    }

}