﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using EnterprisesWebAPI.Models;

namespace EnterprisesWebAPI
{
    public class Persistence
    {
        private MySqlConnection conn;

        public Persistence()
        {
            string myConnectionString;
            myConnectionString = "server=127.0.0.1;uid=root;pwd=mypass;database=enterprisesdb";

            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Investor getEnterprise(long ID)
        {
            Investor p = new Investor();
            MySqlDataReader mysqlReader = null;

            string sqlString = "SELECT * FROM tblpersonnel WHERE ID = " + ID.ToString();
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mysqlReader = cmd.ExecuteReader();
            if (mysqlReader.Read())
            {
                p.id = mysqlReader.GetInt32(0);
                p.investor_name = mysqlReader.GetString(1);
                p.email = mysqlReader.GetString(2);
                p.city = mysqlReader.GetString(3);
                p.country = mysqlReader.GetString(4);
                p.balance = mysqlReader.GetDouble(5);
                p.photo = mysqlReader.GetString(6);
                p.portfolio = null;
                p.portfolio_value = mysqlReader.GetDouble(8);
                p.first_access = mysqlReader.GetBoolean(9);
                p.super_angel = mysqlReader.GetBoolean(10);
                return p;
            }
            else
            {
                return null;
            }
        }

        public ArrayList getEnterprises()
        {
            ArrayList personArray = new ArrayList();

            MySqlDataReader mysqlReader = null;

            string sqlString = "SELECT * FROM enterprises";
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mysqlReader = cmd.ExecuteReader();
            while (mysqlReader.Read())
            {
                Investor p = new Investor();
                p.id = mysqlReader.GetInt32(0);
                p.investor_name = mysqlReader.GetString(1);
                p.email = mysqlReader.GetString(2);
                p.city = mysqlReader.GetString(4);
                p.country = mysqlReader.GetString(5);
                p.balance = mysqlReader.GetDouble(6);
                p.photo = mysqlReader.GetString(7);
                p.portfolio = null;
                p.portfolio_value = mysqlReader.GetDouble(9);
                p.first_access = mysqlReader.GetBoolean(10);
                p.super_angel = mysqlReader.GetBoolean(11);

                personArray.Add(p);
            }

            return personArray;
        }


        public bool deleteEnterprise(long ID)
        {
            Investor p = new Investor();
            MySqlDataReader mysqlReader = null;

            string sqlString = "SELECT * FROM enterprises WHERE ID = " + ID.ToString();
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mysqlReader = cmd.ExecuteReader();

            if (mysqlReader.Read())
            {
                mysqlReader.Close();

                sqlString = "DELETE FROM enterprises WHERE ID = " + ID.ToString();
                cmd = new MySqlCommand(sqlString, conn);

                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }

        public InvestorResponse Login(string email, string password)
        {
            MySqlDataReader mysqlReader = null;

            string sqlString = "SELECT * FROM investor WHERE email = \"" + email + "\"";
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mysqlReader = cmd.ExecuteReader();

           
            if (mysqlReader.Read())
            {
                if (mysqlReader.GetString(3).Equals(password))
                {

                    InvestorResponse inv = new InvestorResponse();

                    inv.id = mysqlReader.GetInt32(0);
                    inv.investor_name = mysqlReader.GetString(1);
                    inv.email = mysqlReader.GetString(2);
                    inv.city = mysqlReader.GetString(4);
                    inv.country = mysqlReader.GetString(5);
                    inv.balance = mysqlReader.GetDouble(6);
                    inv.photo = null;
                    inv.portfolio = null;
                    inv.portfolio_value = mysqlReader.GetDouble(9);
                    inv.first_access = mysqlReader.GetBoolean(10);
                    inv.super_angel = mysqlReader.GetBoolean(11);

                    mysqlReader.Close();

                    return inv;
                }
            }

            mysqlReader.Close();
            return null;
        }
    }
}